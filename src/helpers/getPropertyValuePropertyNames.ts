import "reflect-metadata";
import {
    AutowiredPropertiesMetadataKey, AutowiredPropertiesMetadataPropertyName,
    PropertyValuePropertyNamesMetadataKey, PropertyValuePropertyNamesMetadataPropertyName
} from "../constants";

export function getPropertyValuePropertyNames<T>(obj: T): string[] {
    return Reflect.getMetadata(
            PropertyValuePropertyNamesMetadataKey,
            obj.constructor.prototype,
            PropertyValuePropertyNamesMetadataPropertyName
        ) || [];
}

export default getPropertyValuePropertyNames;