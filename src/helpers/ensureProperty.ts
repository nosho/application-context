export function ensureProperty<T>(obj: T, name: string, propertyDescriptor: PropertyDescriptor): T {
    const _propertyDescriptor = Object.getOwnPropertyDescriptor(obj, name);

    if (_propertyDescriptor) {
        Object.assign(_propertyDescriptor, propertyDescriptor);
    } else {
        Object.defineProperty(obj, name, propertyDescriptor);
    }

    return obj;
}

export default ensureProperty;