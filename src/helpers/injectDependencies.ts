import {ApplicationContext} from "../classes/ApplicationContext";
import {ApplicationContextPropertyName, PropertyValueMetadataKey} from "../constants";
import {ensureProperty} from "./ensureProperty";
import {getAutowiredProperties} from "./getAutowiredProperties";
import {getPropertyValuePropertyNames} from "./getPropertyValuePropertyNames";

export function injectDependencies<T>(obj: T, applicationContextFactory: () => ApplicationContext<any>): T {
    // propagate the application context
    if (!(ApplicationContextPropertyName in obj)) {
        Object.defineProperty(obj, ApplicationContextPropertyName, {get: applicationContextFactory});
    }

    function getApplicationContext(): ApplicationContext<any> | never {
        const applicationContext = applicationContextFactory();
        if (!applicationContext) throw new Error(`Can't inject dependencies without an ApplicationContext`);
        return applicationContext;
    }

    getPropertyValuePropertyNames(obj)
        .forEach(name => {
            const propertyPath: string = Reflect.getMetadata(PropertyValueMetadataKey, obj.constructor.prototype, name)
            ensureProperty(obj, name, {enumerable: true, get: () => getApplicationContext().getProperty(propertyPath)})
        });

    getAutowiredProperties(obj)
        .forEach(name => {
            const type = Reflect.getMetadata(`design:type`, obj.constructor.prototype, name);

            ensureProperty(obj, name, {
                enumerable: true,
                get: () => injectDependencies(getApplicationContext().autowire(type), applicationContextFactory)
            });
        });

    return obj;
}

export default injectDependencies;