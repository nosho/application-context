import "reflect-metadata";
import {AutowiredPropertiesMetadataKey, AutowiredPropertiesMetadataPropertyName} from "../constants";

export function getAutowiredProperties<T>(obj: T): string[] {
    return Reflect.getMetadata(
            AutowiredPropertiesMetadataKey,
            obj.constructor.prototype,
            AutowiredPropertiesMetadataPropertyName
        ) || [];
}

export default getAutowiredProperties;