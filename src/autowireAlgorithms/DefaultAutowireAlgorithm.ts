import {AutowireAlgorithm} from "../classes/AutowireAlgorithm";
import {BeanOptions} from "../classes/ApplicationContext";

export class DefaultAutowireAlgorithm extends AutowireAlgorithm {

    public autowire<T, T2 extends new(...args: any[]) => T>(Class: T2): BeanOptions<T> {
        const eligibleBeans = this.getBeansEligibleForAutowiring(Class);

        if (!eligibleBeans.length) throw new Error(`Could not autowire bean. No bean for class ${Class.name} found. `
            + `Make sure the ${Class.name} class is decorated with the @Bean() decorator and `
            + `the necessary profiles are active/inactive on the application context.`);

        if (eligibleBeans.length > 1) {
            // Multiple eligible beans found, see if there is one that is more eligible.
            // If the most eligible bean has an equally eligible bean, throw an error because we can't determine which
            // bean should be used in that case.
            // Eligibility is based on the amount of matching profiles, the more matching profiles,
            // the more eligible the bean
            if (this.getMatchingProfiles(eligibleBeans[0]).length === this.getMatchingProfiles(eligibleBeans[1]).length) {
                throw new Error(`Could not autowire bean for class ${Class.name}, `
                    + `multiple beans that are equally eligible have been found. `);
            }
        }

        return eligibleBeans[0];
    }

    protected getBeansEligibleForAutowiring<T, T2 extends new(...args: any[]) => T>(Class: T2) {
        return this.applicationContext.getBeanDefinitions()
            .filter(bean => this.beanIsActiveAccordingToTypes(Class, bean))
            .filter(bean => this.beanIsActiveAccordingToProfiles(bean))
            // sort by eligibility
            .sort((a, b) => this.getMatchingProfiles(b).length - this.getMatchingProfiles(a).length);
    }

    protected beanIsActiveAccordingToTypes<T, T2 extends new(...args: any[]) => T>(Class: T2, bean: BeanOptions<T>): boolean {
        if (Array.isArray(bean.types)) {
            return bean.types.some(type => (Class === type || type.prototype instanceof Class));
        } else {
            return bean.types === Class || Class.prototype instanceof bean.types
        }
    }

    protected beanIsActiveAccordingToProfiles<T>(bean: BeanOptions<T>): boolean {
        // beans without profiles are always active
        if (!bean.profiles || !bean.profiles.length) return true;

        let eligible: boolean = false;
        let decided: boolean = false;

        // If a bean has profiles, only one has to be active.
        // Profiles that start with an exclamation point indicate this bean may NOT be active when that profile is.
        // Profiles that end with an exclamation point indicate this bean is only active when the profile is.
        bean.profiles.forEach(profile => {
            if (decided) return;

            const _eligible: boolean = this.beanProfileIsActive(profile);

            if (profile.startsWith("!") || profile.endsWith("!")) {
                eligible = _eligible;
                // if an inclusive or exclusive profile isn't matched, the bean isn't be eligible
                if (!eligible) decided = true;
            } else if (!eligible) {
                // if a normal profile isn't active on a bean that is already eligible it doesn't override its
                // eligibility
                eligible = _eligible;
            }
        });

        return eligible;
    }

}

export default DefaultAutowireAlgorithm;