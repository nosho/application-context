import {ApplicationContext, BeanOptions} from "./ApplicationContext";

export abstract class AutowireAlgorithm {

    protected applicationContext: ApplicationContext<any>;

    constructor(applicationContext: ApplicationContext<any>) {
        this.applicationContext = applicationContext;
    }

    public abstract autowire<T, T2 extends new(...args: any[]) => T>(Class: T2): T;

    protected beanProfileIsActive<T>(profile: string): boolean {
        switch (true) {
            case profile.startsWith("!"):
                return !this.applicationContext.hasActiveProfile(profile.substr(1));

            case profile.endsWith("!"):
                return this.applicationContext.hasActiveProfile(profile.substr(0, profile.length - 1));

            default:
                return this.applicationContext.hasActiveProfile(profile);
        }
    }

    protected getMatchingProfiles<T>(bean: BeanOptions<T>): string[] {
        return (bean.profiles || []).filter(profile => this.beanProfileIsActive(profile));
    }

}

export default AutowireAlgorithm;