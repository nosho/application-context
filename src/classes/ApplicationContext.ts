import "reflect-metadata";
import {DefaultAutowireAlgorithm} from "../autowireAlgorithms/DefaultAutowireAlgorithm";
import {BeanDefinitionsMetadataKey} from "../constants";

export interface BeanOptions<T> {
    types?: (new(...args: any[]) => T) | (new(...args: any[]) => T)[];
    factory?(applicationContext?: ApplicationContext<any>): T;
    singleton?: boolean;
    profiles?: string[]
}

export type ApplicationContextProperties = { [name: string]: ApplicationContextProperty };
export type ApplicationContextProperty = string | number | boolean;

export type RemoveChangeListener = () => void;
export type ChangeListener = () => void;

export class ApplicationContext<Properties extends ApplicationContextProperties> {

    protected profiles: string[] = [];
    protected properties: Properties = {} as Properties;
    protected singletonsBeans: BeanOptions<any>[] = [];
    protected singletons: any[] = [];
    protected changeListeners: ChangeListener[] = [];
    protected autowireAlgorithm = new DefaultAutowireAlgorithm(this);

    protected beanDefinitions: BeanOptions<any>[] = [
        ...(Reflect.getMetadata(BeanDefinitionsMetadataKey, this.constructor) || []), {
            types: [ApplicationContext],
            factory: () => this,
            singleton: true
        }
    ];

    public addChangeListener(changeListener: ChangeListener): RemoveChangeListener {
        let removed: boolean = false;

        this.changeListeners.push(changeListener);

        return () => {
            if (removed) return;

            // we can't remove by index because it can change due to other changeListeners being removed
            this.changeListeners = this.changeListeners.filter(_changeListener => _changeListener !== changeListener);

            removed = true;
        };
    }

    public getProperty(path: string): ApplicationContextProperty {
        return this.properties[path];
    }

    public setProperties(properties: Partial<Properties>): void {
        Object.assign(this.properties, properties)
        this.emitChange();
    }

    public setProperty(name: string, value: ApplicationContextProperty): void {
        this.properties[name] = value;
        this.emitChange();
    }

    public setActiveProfiles(...profiles: string[]): void {
        this.profiles = profiles;
        this.emitChange();
    }

    public addActiveProfiles(...profiles: string[]): void {
        this.profiles = [...this.profiles, ...profiles]
        this.emitChange();
    }

    public hasActiveProfile(profile: string): boolean {
        return this.profiles.indexOf(profile) !== -1;
    }

    public autowire<T, T2 extends new(...args: any[]) => T>(Class: T2): T {
        const bean = this.autowireAlgorithm.autowire<T, T2>(Class);

        return bean.singleton
            ? this.ensureSingleton(bean)
            : bean.factory(this)
    }

    public getBeanDefinitions(): BeanOptions<any>[] {
        // return a clone of the array
        return [...this.beanDefinitions];
    }

    protected emitChange(): void {
        // Reset singletons when something changes because a different bean might be eligible for autowiring.
        // Because of this, beans should ALWAYS be stateless, as their state will be lost when singletons are reset.
        this.singletons = [];
        this.singletonsBeans = [];
        this.changeListeners.forEach(fn => fn());
    }

    protected ensureSingleton<T>(bean: BeanOptions<T>): T {
        const singletonIndex = this.singletonsBeans.indexOf(bean);

        return singletonIndex === -1
            ? this.addSingleton(bean)
            : this.singletons[singletonIndex];
    }

    protected addSingleton<T>(bean: BeanOptions<T>): T {
        const singleton = bean.factory(this);
        this.singletonsBeans.push(bean);
        this.singletons.push(singleton);
        return singleton;
    }

}

export default ApplicationContext;