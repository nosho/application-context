import "reflect-metadata";
import {
    AutowireDefinitionMetadataKey,
    AutowiredPropertiesMetadataKey,
    AutowiredPropertiesMetadataPropertyName
} from "../constants";

export type AutowiredDecorator = PropertyDecorator;

const BuiltIns = [Number, Object, Function, Date, Boolean, String, Promise];

export const Autowired: AutowiredDecorator = (target: any, propertyName: string) => {
    const type = Reflect.getMetadata("design:type", target, propertyName);
    if (!type) throw new Error(`You need to provide a type when autowiring.`);
    if (BuiltIns.indexOf(type) !== -1) throw new Error(`You can only autowire (non-builtin) classes, not interfaces or types.`);
    const autowiredProperties = Reflect.getMetadata(AutowiredPropertiesMetadataKey, target, AutowiredPropertiesMetadataPropertyName) || [];

    Reflect.defineMetadata(AutowiredPropertiesMetadataKey, [...autowiredProperties, propertyName], target, AutowiredPropertiesMetadataPropertyName)
    Reflect.defineMetadata(AutowireDefinitionMetadataKey, {type}, target, propertyName);
};

export default Autowired;