import "reflect-metadata";
import {ApplicationContext, BeanOptions} from "../classes/ApplicationContext";
import {BeanDefinitionMetadataKey, BeanDefinitionsMetadataKey} from "../constants";
import {injectDependencies} from "../helpers/injectDependencies";

export type BeanDecorator = ClassDecorator & (<T>(options?: BeanOptions<T>) => ClassDecorator);

function decorateBean<T>(target: new(...args: any[]) => T, options: BeanOptions<T> = {} as BeanOptions<T>) {
    options.types = options.types || [target];

    // ensure types is an array
    if (!Array.isArray(options.types)) options.types = [options.types];

    // ensure target is in types array
    if (options.types.indexOf(target) === -1) options.types.push(target);

    // default singleton to true
    options.singleton = options.singleton == null ? true : options.singleton;

    // set default factory
    options.factory = options.factory || ((applicationContext) => {
            function BeanConstructor(...args: any[]): void {
                // inject dependencies before anything else
                injectDependencies(this, () => applicationContext);

                const instance = target.call(this, ...args) || this;

                // set original constructor on instance for instanceof checks
                instance.constructor = target;

                return instance;
            }

            Object.defineProperty(BeanConstructor, "name", {writable: true});

            // inherit prototype and name
            BeanConstructor.prototype = target.prototype;
            (BeanConstructor as any).name = target.name;

            return new BeanConstructor();
        });

    // define beanDefinition 'globally', on the ApplicationContext class
    Reflect.defineMetadata(BeanDefinitionsMetadataKey, [
        ...(Reflect.getMetadata(BeanDefinitionsMetadataKey, ApplicationContext) || []),
        options
    ], ApplicationContext);

    // define beanDefinition on class
    Reflect.defineMetadata(BeanDefinitionMetadataKey, options, target);

}

export const Bean: BeanDecorator = (<T>(options: BeanOptions<T> = {} as BeanOptions<T>): ClassDecorator | void => {
    return typeof options === "function"
        ? decorateBean(options)
        : (target: new(...args: any[]) => T) => decorateBean(target, options);
}) as any;

export default Bean;