import "reflect-metadata";
import {
    PropertyValueMetadataKey,
    PropertyValuePropertyNamesMetadataKey,
    PropertyValuePropertyNamesMetadataPropertyName
} from "../constants";

export function PropertyValue(path: string): PropertyDecorator {
    return (target: any, propertyName: string): void => {
        const propertyValueProperties = Reflect.getMetadata(PropertyValuePropertyNamesMetadataKey, target, PropertyValuePropertyNamesMetadataPropertyName) || [];

        Reflect.defineMetadata(PropertyValueMetadataKey, path, target, propertyName);
        Reflect.defineMetadata(PropertyValuePropertyNamesMetadataKey, [...propertyValueProperties, propertyName], target, PropertyValuePropertyNamesMetadataPropertyName);
    };
}

export default PropertyValue;