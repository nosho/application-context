import * as React from "react";

export const ApplicationContextPropTypes = {
    applicationContext: React.PropTypes.object.isRequired
};

export const BeanDefinitionsMetadataKey = "application-context:beanDefinitions";
export const BeanDefinitionMetadataKey = "application-context:beanDefinition";
export const AutowiredPropertiesMetadataKey = "application-context:autowiredProperties";
export const AutowireDefinitionMetadataKey = "application-context:autowireDefinition";
export const PropertyValueMetadataKey = "application-context:propertyValue";
export const PropertyValuePropertyNamesMetadataKey = "application-context:propertyValuePropertyNames";

export const ApplicationContextPropertyName = "__APPLICATION_CONTEXT__";
export const AutowiredPropertiesMetadataPropertyName = "__AUTOWIRED_PROPERTY_NAMES__";
export const PropertyValuePropertyNamesMetadataPropertyName = "__PROPERTY_VALUE_PROPERTY_NAMES__";
