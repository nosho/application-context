import * as React from "react";
import {ApplicationContext, ApplicationContextProperties} from "../classes/ApplicationContext";
import {ApplicationContextPropTypes} from "../constants";

export interface ApplicationContextProviderContext<T extends ApplicationContextProperties> {
    applicationContext: ApplicationContext<T>;
}

export interface ApplicationContextProviderProps<Properties extends ApplicationContextProperties> {
    profiles?: string[]
    properties?: Properties;
}

export class ApplicationContextProvider<Properties extends ApplicationContextProperties> extends React.Component<ApplicationContextProviderProps<Properties>, void> {

    //noinspection JSUnusedGlobalSymbols
    public static childContextTypes = ApplicationContextPropTypes;

    private applicationContext: ApplicationContext<Properties>;

    constructor(props: ApplicationContextProviderProps<Properties>, context: ApplicationContextProviderContext<Properties>) {
        const applicationContext = new ApplicationContext<Properties>();

        // initialize application context before calling super
        if (props.profiles && props.profiles.length) applicationContext.setActiveProfiles(...props.profiles);
        if (props.properties) applicationContext.setProperties(props.properties);

        super(props, context);

        this.applicationContext = applicationContext;
    }

    //noinspection JSUnusedGlobalSymbols
    public getChildContext(): ApplicationContextProviderContext<Properties> {
        return {applicationContext: this.applicationContext};
    }

    public render(): JSX.Element {
        const childCount = React.Children.count(this.props.children);

        if (!childCount) {
            return null;
        } else if (childCount === 1) {
            return React.Children.only(this.props.children);
        } else {
            return <div className="application-context-provider">{this.props.children}</div>;
        }
    }

}

export default ApplicationContextProvider;